# TODO

## State

- We are able to create a subnet using a bridge:
  - masquerading `10.100.0.0` to `vmbr0`.
  - bridge addr: `10.100.0.1/16`.
  - gateway: `10.100.0.1`.
  - currently using `8.8.8.8` as nameserver. To be replaced with CoreDNS.

## Next steps

- [ ] create a DNS server with CoreDNS

## Clean things to do

- [ ] create template of alpine
- [ ] create template of amazon linux 2
- [ ] 