#!/bin/ash

####################### ARGS #######################

hostname=
# network
net_ip_addr=
net_gateway=
# ns
ns_name=
ns_ip=

###################### FLAGS #######################

while [[ $# -gt 0 ]]; do
  case $1 in
    --name)
      hostname=$2
      shift;shift;;
    --name=*)
      hostname=${1#*=}
      shift;;
    --ip)
      net_ip_addr=$2
      shift;shift;;
    --ip=*)
      net_ip_addr=${1#*=}
      shift;;
    --gateway)
      net_gateway=$2
      shift;shift;;
    --gateway=*)
      net_gateway=${1#*=}
      shift;;
    --ns-ip)
      ns_ip=$2
      shift;shift;;
    --ns-ip=*)
      ns_ip=${1#*=}
      shift;;
    --ns-name)
      ns_name=$2
      shift;shift;;
    --ns-name=*)
      ns_name=${1#*=}
      shift;;
    -*)
      echo "Unknown option $1"
      exit 1;;
    *)
      echo "This script does not take any argument"
      exit 1;;
  esac
done

if [ -z "$hostname" ] || [ -z "$net_ip_addr" ] || [ -z "$net_gateway" ] || [ -z "$ns_name" ] || [ -z "$ns_ip" ]; then
  echo "please specify \`hostname\`, \`net_ip_addr\`, \`net_gateway\`, \`ns_name\` and \`ns_ip\`"
fi

##################### FILEPATH #####################

hostname_fp=/etc/hostname
hosts_fp=/etc/hosts
net_interfaces_fp=/etc/network/interfaces
resolv_conf_fp=/etc/resolv.conf

###################### CONFIG ######################

resolv_conf="search $ns_name
nameserver $ns_ip
nameserver 8.8.8.8
nameserver $net_gateway"

network_interfaces="auto lo
iface lo inet loopback
auto eth0
iface eth0 inet static
        address $net_ip_addr
        gateway $net_gateway"

hostname_fqdn="$hostname.$ns_name"

hosts="127.0.0.1       $hostname_fqdn $hostname localhost.localdomain localhost
::1             localhost localhost.localdomain"

#################### PROCEDURE ####################

# keyboard
setup-keymap us us

# hosts
echo "$hostname_fqdn" > "$hostname_fp"
echo "$hosts" > "$hosts_fp"
rc-service hostname restart # or /etc/init.d/hostname restart

# networking
echo "$resolv_conf" > "$resolv_conf_fp"
echo "$network_interfaces" > "$net_interfaces_fp"
rc-service networking start
rc-update add networking boot

# timezone
setup-timezone -z CET

# repositories
setup-apkrepos -1

# ssh
apk add openssh
rc-service openssh start
rc-update add openssh
sed -i '.*PermitRootLogin.*/PermitRootLogin yes' /etc/ssh/sshd_config
service sshd restart

# ntp
apk add chronyd
rc-service chronyd start
rc-update add chronyd

# disks
setup-disk -m sys /dev/sda

# setup pass
passwd

# reboot