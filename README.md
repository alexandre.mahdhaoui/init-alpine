# init-alpine

## Repository structure

| Path     | Description               |
|----------|---------------------------|
| `script` | contains init bash script |

## Getting started

Please note that `--ip` must be of the format: `10.0.0.5/24`.

```shell
init.sh --name=<host name> --ip=10.0.0.5/24  --gateway=10.0.0.2 --ns-ip=10.0.0.3 --ns-name=example.com
```

## Installing k3s

#### Run the prerequisites:
```shell
k3s_prereq.sh
```
#### After reboot:
```shell
cni.sh
```

#### Reboot & run:
```shell
k3s.sh
```